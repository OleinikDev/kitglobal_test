const Product = require('../models/Product');
const path = require('path');

class ProductController {
  async addProduct(req, res) {
    try {
      const { name, price, description } = req.body;
      const product = new Product({ name, price, description });
      
      await product.save();
      return res.json(product);
    } catch (e) {
      console.log(e);
      return res.status(400).json(e);
    }
  }

  async getProducts(req, res) {
    try {
      const product = await Product.find();
      
      return res.json(product);
    } catch (e) {
      console.log(e);
      return res.status(500).json({ message: "Can not get products" })
    }
  }

  async deleteProduct(req, res) {
    try {
      const { id } = req.params;
      const product = await Product.findOne({ _id: id });
      if (!product) {
        return res.status(400).json({ message: 'Product not found' });
      }
      await product.remove();
      return res.json({ message: 'Product was deleted' });
    } catch (e) {
      return res.status(500).json({ message: "Can not delete product" })
    }
  }
};

module.exports = new ProductController();
