const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const DB_URL = require('./constants/index.js');
const productRouter = require('./routes/index.js');

const app = express();
const PORT = process.env.PORT || 8181;

app.use(express.json());
app.use(cors({
    origin: '*',
    methods: ['GET','POST','DELETE','UPDATE','PUT','PATCH']
}));
app.use("/api/products", productRouter);

const start = async () => {
  try {

      await mongoose.connect(DB_URL, { useUnifiedTopology: true, useNewUrlParser: true });

      app.listen(PORT, () => {
          console.log(`🔥: Server start on port: ${PORT}`);
      });

  } catch (e) {
      console.log(e);
  }
}

start();