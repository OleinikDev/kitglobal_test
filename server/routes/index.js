
const Router = require('express');
const product = require('../controllers/ProductController');

const productRouter = new Router();

productRouter.post('/add', product.addProduct);
productRouter.get('', product.getProducts);
productRouter.delete('/:id', product.deleteProduct);

module.exports  = productRouter