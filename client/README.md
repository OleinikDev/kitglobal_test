# CLIENT SIDE #

## How to start FE-part localy:

1. In terminal run command
> **npm instal**

2. Next commands for runing project:
* For local developing: **npm run start:dev**
* For production: **npm run start:prod**

3. Open link [http://localhost:3000](http://localhost:3000)


## How use API:

1. If you use local setup, you should:
   - Go to the **server** folder and run command in terminal
     > **npm run start:dev**
   - Server started on **http://localhost:8080/**
2. If you use prod setup, you should use API URL: **https://kitglobal-test.onrender.com/**

## API ROUTES:
1. **/api/products** - return all products 
    - Method: **GET**
    - Params: **-**

2. **/api/products/add** - add one product
    - Method: **POST**
    - Params: ``{name: "Product name", price: "100", description: "Short description"}``

3. **/api/products/:id** - delete one product by ID
    - Method: **DELETE**
    - Params: **-**

