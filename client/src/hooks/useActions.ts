import { backetActions } from '@/store/reducers/backetSlice';
import { modalStatusActions } from '@/store/reducers/modalActions';
import { bindActionCreators } from '@reduxjs/toolkit';
import { useAppDispatch } from './userAppDispatch';

const actions = {
  ...modalStatusActions,
  ...backetActions
}

export const useActions = () => {
  const dispatch = useAppDispatch();
  return bindActionCreators(actions, dispatch);
}