export type ProductProps = {
  name: string;
  price: string;
  description: string;
  _id: string;
  items: number;
}