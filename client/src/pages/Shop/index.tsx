import React from 'react';
import { useEffect } from 'react'
import { getProducts } from '@/store/actions/getProducts';
import { useAppDispatch } from '@/hooks/userAppDispatch';
import { useSelector } from 'react-redux';
import { selectAllProducts, selectAllProductsStatus } from '@/store/selectors/getAllProducts';
import { ShopWrapper } from './styles';
import ProductCard from '@/components/ProductCard';
import Loader from '@/components/Loader';
import { ProductProps } from './types';

const Shop = () => {
  const dispatch = useAppDispatch();
  const allProducts = useSelector(selectAllProducts);
  const isProducts = useSelector(selectAllProductsStatus);

  useEffect(() => {
    dispatch(getProducts());
  }, []);

  return (
    <ShopWrapper>
      {!isProducts
        ? <Loader />
        : allProducts.map((product: ProductProps) => <ProductCard product={product} key={product._id} />)
      }
    </ShopWrapper>
  );
};

export default Shop;