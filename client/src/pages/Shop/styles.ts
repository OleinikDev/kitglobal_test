import styled from 'styled-components';

export const ShopWrapper = styled.section`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
`;