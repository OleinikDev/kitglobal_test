import { ProductProps } from "../Shop/types";

export const getTotalSum = (products: ProductProps[]) => {
  return Array.isArray(products) && products.length !== 0 ? products.reduce((total, item) => total + (Number(item.price) * item.items), 0) : 0;
}