import BacketTable from '@/components/BacketTable';
import { selectBacketProducts } from '@/store/selectors/getBacketProducts';
import React from 'react';
import { useSelector } from 'react-redux';
import { getTotalSum } from './helpers';
import { BacketWrapp } from './styles';

const Backet = () => {
  const products = useSelector(selectBacketProducts);
  const total = getTotalSum(products);

  return (
    <BacketWrapp>
      <BacketTable products={products} />
      <p>Total: {total}</p>
    </BacketWrapp>
  );
};

export default Backet;