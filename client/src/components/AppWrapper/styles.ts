import styled from 'styled-components';

export const Wrapper = styled.section`
  width: 1200px;
  min-height: 100vh;
  background: #fff;
  padding: 20px;
  box-sizing: border-box;
  color: #333333;
`;
