import React from 'react';
import Navigation from '@/components/Navigation';
import { Wrapper } from './styles';
import { AppWrapperProps } from './types';
import { useSelector } from 'react-redux';
import { selectModalStatus } from '@/store/selectors/getModalStatus';
import AddProductModal from '../AddProductModal';

const AppWrapper = ({ children }: AppWrapperProps) => {
  const isModalOpen = useSelector(selectModalStatus);

  return (
    <>
      <Wrapper>
        <Navigation />
        {children}
      </Wrapper>
      {isModalOpen && <AddProductModal />}
    </>
  );
};

export default AppWrapper;