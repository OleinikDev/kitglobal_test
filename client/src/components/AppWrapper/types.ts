export type AppWrapperProps = {
  children: JSX.Element[] | JSX.Element
};