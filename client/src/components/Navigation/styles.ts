import styled from 'styled-components';

export const NavigationWrapp = styled.nav`
  width: 100%;
  height: 60px;
  margin-bottom: 40px;
  display: flex;
  align-items: center;
`;

export const NavigationList = styled.ul`
  display: flex;
  align-items: center;
  list-style: none;
  padding-left: 0;
  & li {
    font-size: 16px;
    margin-right: 20px;
    font-weight: 500;
    cursor: pointer;
    &:hover {
      color: #fa541c;
      transition: all .2s ease-in-out;
    }
    &.logo {
      font-size: 18px;
      margin-right: 50px;
    }
  }
`