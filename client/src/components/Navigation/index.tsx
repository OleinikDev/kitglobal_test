import React from 'react';
import { NavigationList, NavigationWrapp } from './styles';
import { DropboxOutlined } from '@ant-design/icons';
import { Link } from 'react-router-dom';
import { Button } from '../UI/Button';
import { useActions } from '@/hooks/useActions';

const Navigation = () => {
  const { setModalOpen } = useActions();
  return (
    <NavigationWrapp>
      <NavigationList>
        <li className='logo'>
          <Link to="/"><DropboxOutlined /> SomeShop</Link>
        </li>
        <li><Link to="/">Shop</Link></li>
        <li><Link to="/backet">Backet</Link></li>
        <li><Button onClick={() => setModalOpen()}>Add product</Button></li>
      </NavigationList>
    </NavigationWrapp>
  );
};

export default Navigation;