import styled from 'styled-components';

export const Modal = styled.div`
  position: fixed;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 100%;
  left: 0;
  top: 0;
  z-index: 10;
  background: rgba(0,0,0,0.5);
`;

export const ModalContent = styled.div`
  width: 400px;
  height: 400px;
  background: #fff;
  border-radius: 10px;
  color: #333333;
  position: relative;
  padding: 40px 20px 20px;
  box-sizing: border-box;

  & .close-wrapp {
      position: absolute;
      right: 20px;
      top: 20px;
      font-size: 24px;
      transition: all .2s cubic-bezier(.645,.045,.355,1);
      &:hover {
        color: #cf1322;
      }
    }
  }
`;


