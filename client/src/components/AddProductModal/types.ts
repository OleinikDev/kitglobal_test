export type FormValues = {
  name: string;
  price: string;
  description: string;
}