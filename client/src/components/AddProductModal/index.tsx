import React from 'react';
import { CloseCircleOutlined } from '@ant-design/icons';
import { Modal, ModalContent } from './styles';
import { useActions } from '@/hooks/useActions';
import { Formik, Form } from 'formik';
import { initialValues, ValidationSchema } from './helpers';
import Input from '../UI/Input';
import { useAppDispatch } from '@/hooks/userAppDispatch';
import { addProducts } from '@/store/actions/addProducts';
import { FormValues } from './types';
import { getProducts } from '@/store/actions/getProducts';

const AddProductModal = () => {
  const { setModalClose } = useActions();
  const dispatch = useAppDispatch();

  const handleSubmit = (values: FormValues) => {
    dispatch(addProducts(values));
    setModalClose();
    dispatch(getProducts());
  };

  return (
    <Modal>
      <ModalContent>
        <div className="close-wrapp">
          <CloseCircleOutlined onClick={() => setModalClose()} />
        </div>
        <Formik
          initialValues={initialValues}
          validationSchema={ValidationSchema}
          onSubmit={handleSubmit}
        >
          {({ errors, touched, handleChange, values }) => (
            <Form>
              <Input
                label="Name"
                name="name"
                required
                placeholder="Product name"
                error={errors.name && touched.name ? errors.name : ''}
                onChange={handleChange}
                value={values.name}
              />
              <Input
                label="Price"
                name="price"
                required
                placeholder="Product price"
                error={errors.price && touched.price ? errors.price : ''}
                onChange={handleChange}
                value={values.price}
              />
              <Input
                label="Description"
                name="description"
                required
                placeholder="Product description"
                error={errors.description && touched.description ? errors.description : ''}
                onChange={handleChange}
                value={values.description}
              />
              <button type="submit">Submit</button>
            </Form>
          )}
        </Formik>
      </ModalContent>
    </Modal>
  );
};

export default AddProductModal;