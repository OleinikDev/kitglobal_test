import * as Yup from 'yup';

export const ValidationSchema = Yup.object().shape({
  name: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
  price: Yup.number()
    .typeError('Must be number!')
    .min(2, 'Too Short!')
    .max(1000, 'Too Long!')
    .required('Required'),
  description: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
});

export const initialValues = {
  name: '',
  price: '',
  description: '',
}