import React from 'react';
import { Spinner } from './styles';

const Loader = () => {
  return (
    <Spinner>
      <div className='loader'></div>
    </Spinner>
  );
};

export default Loader;