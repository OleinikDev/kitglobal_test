import styled from 'styled-components';

export const Card = styled.div`
  width: 300px;
  height: auto;
  border-radius: 10px;
  background: #fff;
  border: 1px solid #333333;
  padding: 15px;
  margin: 0px 15px 30px 15px;
  
  & .image {
    font-size: 150px;
    display: flex;
    align-items: center;
    justify-content: center;
  }

  & span {
    font-weight: 600;
  }
`;

export const InCart = styled.span`
  box-sizing: border-box;
  margin: 0;
  font-size: 16px;
  padding: 10px 0px;
  color: #389e0d;
  line-height: 20px;
  list-style: none;
  display: flex;
  justify-content: center;
  height: auto;
  margin-inline-end: 8px;
  white-space: nowrap;
  background: #f6ffed;
  border: 1px solid #d9d9d9;
    border-top-color: rgb(217, 217, 217);
    border-right-color: rgb(217, 217, 217);
    border-bottom-color: rgb(217, 217, 217);
    border-left-color: rgb(217, 217, 217);
  border-radius: 4px;
  opacity: 1;
  transition: all .2s;
  border-color: #b7eb8f;
`;

