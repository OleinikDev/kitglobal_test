import React from 'react';
import { Card, InCart } from './styles';
import { FileImageOutlined } from '@ant-design/icons';
import { Button } from '../UI/Button';
import { ProductProps } from '@/pages/Shop/types';
import { useAppDispatch } from '@/hooks/userAppDispatch';
import { deleteProduct } from '@/store/actions/deleteProduct';
import { useActions } from '@/hooks/useActions';
import { useSelector } from 'react-redux';
import { selectBacketProducts } from '@/store/selectors/getBacketProducts';

const ProductCard = ({ product }: ProductProps) => {
  const { name, price, description } = product;
  const dispatch = useAppDispatch();
  const productCart = useSelector(selectBacketProducts);
  const { addProductToBacket } = useActions();

  const deleteBtnHelper = () => {
    dispatch(deleteProduct(product._id));
  }

  const isInCart = productCart.some((prod: ProductProps) => prod._id === product._id);

  return (
    <Card>
      <div className='image'>
        <FileImageOutlined style={{ fontSize: "150px" }} />
      </div>
      <p>Name: <span>{name}</span></p>
      <p>Price: <span>{price}$</span></p>
      <p>Description: <span>{description}</span></p>
      {isInCart
        ? <InCart>In Cart</InCart>
        : <Button onClick={() => addProductToBacket({ ...product, items: 1 })}>Add to cart</Button>
      }
      <Button marginTop={"10"} styleType={'delete'} onClick={() => deleteBtnHelper()}>Delete product from shop</Button>
    </Card>
  );
};

export default ProductCard;