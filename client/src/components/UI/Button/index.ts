import styled from "styled-components";

interface Props {
  marginTop?: string
  styleType?: string;
}

export const Button = styled.button`
  border: none;
  height: 32px;
  padding: 4px 15px;
  border-radius: 6px;
  display: flex;
  align-items: center;
  justify-content: center;
  transition: all .2s cubic-bezier(.645,.045,.355,1);
  color: #fff;
  background-color: ${({ styleType }) => (styleType === "delete" ? "#ff4d4f" : "#1677ff")};
  box-shadow: ${({ styleType }) => (styleType === "delete" ? "0 2px 0 rgba(255,38,5,.06);" : "0 2px 0 rgba(5,145,255,.1)")};
  margin-top ${({ marginTop }: Props) => (`${marginTop}px` || 0)};
  &:hover {
    background-color: ${({ styleType }) => (styleType === "delete" ? "#ff7875" : "#4096ff")};
  }
`