import React from 'react';
import { InputWrapper, StyledInput } from './styles';
import { InputProps } from './types';

const Input = ({ placeholder, required, error, label, onChange, name, value }: InputProps) => {
  return (
    <InputWrapper>
      <div>{label} {required && <span className='required'>*</span>}</div>
      <StyledInput
        placeholder={placeholder}
        onChange={onChange}
        name={name}
        value={value}
        id={name}
        type="text"
      />
      {error && (
        <div className='error'>
          {error}
        </div>
      )
      }
    </InputWrapper>
  );
};

export default Input;