import styled from "styled-components";

export const InputWrapper = styled.div`
  margin-bottom: 20px;

  & .required {
    color: #cf1322;
  }

  & .error {
    font-size: 12px;
    color: #cf1322;
  }
`

export const StyledInput = styled.input`
  box-sizing: border-box;
  margin: 0;
  padding: 4px 11px;
  color: rgba(0,0,0,.88);
  font-size: 14px;
  line-height: 1.5;
  list-style: none;
  position: relative;
  display: inline-block;
  width: 100%;
  min-width: 0;
  background-color: #fff;
  background-image: none;
  border-width: 1px;
  border-style: solid;
  border-color: #d9d9d9;
  border-radius: 6px;
  transition: all .2s;
`