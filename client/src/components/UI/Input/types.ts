import { ChangeEvent } from "react";

export type InputProps = {
  placeholder: string;
  required: boolean;
  error: string;
  label: string;
  onChange: (e?: ChangeEvent<any>) => void;
  name: string;
  value: string;
}