import { useActions } from '@/hooks/useActions.js';
import { ProductProps } from '@/pages/Shop/types.js';
import { Table, TableDelBtn, TableRow, TableRowCounter, TableRowHeader } from './styles.js'
import { PlusOutlined, MinusOutlined } from '@ant-design/icons';

const BacketTable = ({ products }: ProductProps[]) => {
  const { deleteProductFromBacket, incAmount, decAmount } = useActions();

  const deleteBtnHandler = (id: string) => {
    if (confirm('Are you shure?') === true) {
      deleteProductFromBacket(id);
    }
  }

  return (
    <Table>
      <TableRowHeader>
        <div>Name</div>
        <div>Count</div>
        <div>Price</div>
        <div>Actions</div>
      </TableRowHeader>
      {products.map((product: ProductProps) => {
        const price = Number(product.price) * product.items;
        return (
          <TableRow key={product._id}>
            <div>{product.name}</div>
            <TableRowCounter>
              <button type='button' onClick={() => decAmount(product)} >
                <MinusOutlined />
              </button>
              <input type="text" value={product.items} id={product._id} name={product._id} readOnly />
              <button type='button' onClick={() => incAmount(product)} >
                <PlusOutlined />
              </button>
            </TableRowCounter>
            <div>{price}</div>
            <div><TableDelBtn onClick={() => deleteBtnHandler(product._id)}>Delete</TableDelBtn></div>
          </TableRow>
        )
      })}
    </Table >
  );
};

export default BacketTable;