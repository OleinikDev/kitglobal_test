import styled from 'styled-components';

export const Table = styled.div`
  width: 100%;
  padding: 15px;
  box-sizing: border-box;
  border-radius: 10px;
  border: 1px solid #333333;
  display: flex;
  flex-direction: column;
`;

export const TableRowHeader = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  background: #fafafa;
  color: rgba(0,0,0,.88);
  font-weight: 600; 
  border-bottom: 1px solid #f0f0f0;
  & div {
    display: flex;
    align-items: flex-start;
    width: 100%;
    padding: 15px;
    box-sizing: border-box;
  }

  & div:not(:last-of-type) {
    border-right: 1px solid #f0f0f0;
  }
`;


export const TableRow = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  border-bottom: 1px solid #f0f0f0;
  & div {
    display: flex;
    align-items: flex-start;
    width: 100%;
    padding: 15px;
    box-sizing: border-box;
  }

  & div:not(:last-of-type) {
    border-right: 1px solid #f0f0f0;
  }
`;

export const TableDelBtn = styled.button`
  box-sizing: border-box;
  font-weight: 500;
  margin: 0;
  padding: 5px 0px;
  color: #d4380d;
  font-size: 14px;
  line-height: 20px;
  list-style: none;
  display: flex;
  height: auto;
  margin-inline-end: 8px;
  padding-inline: 30px;
  white-space: nowrap;
  background: #fff2e8;
  border: 1px solid #d9d9d9;
    border-top-color: rgb(217, 217, 217);
    border-right-color: rgb(217, 217, 217);
    border-bottom-color: rgb(217, 217, 217);
    border-left-color: rgb(217, 217, 217);
  border-radius: 4px;
  opacity: 1;
  transition: all .2s;
  text-align: start;
  border-color: #ff4d4f;
  &:hover {
    border-color: #ff4d4f;
    color: #fff;
    background: #ff4d4f;
  }
`
export const TableRowCounter = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;

  & input {
    width: 100px;
    box-sizing: border-box;
    margin: 0 5px;
    padding: 4px 11px;
    color: rgba(0,0,0,.88);
    font-size: 14px;
    line-height: 1.5;
    position: relative;
    display: inline-block;
    min-width: 0;
    background-color: #fff;
    background-image: none;
    border: 1px solid #d9d9d9;
    border-radius: 6px;
    transition: all .2s;
    text-align: center;
  }

  & button {
    outline: none;
    position: relative;
    display: inline-block;
    font-weight: 400;
    white-space: nowrap;
    text-align: center;
    background-image: none;
    border: 1px solid transparent;
    cursor: pointer;
    transition: all .2s cubic-bezier(.645,.045,.355,1);
    user-select: none;
    touch-action: manipulation;
    line-height: 1.5;
    font-size: 14px;
    height: 30px;
    width: 40px;
    padding: 4px 10px;
    border-radius: 6px;
    color: #fff;
    background-color: #1677ff;
    box-shadow: 0 2px 0 rgba(5,145,255,.1);
  }
`