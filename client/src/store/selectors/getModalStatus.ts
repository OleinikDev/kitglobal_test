import { createSelector } from 'reselect';

const template = (state: any) => state.modal;

export const selectModalStatus = createSelector(template, ({ open }) => open);