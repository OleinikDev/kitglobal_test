import { createSelector } from 'reselect';

const template = (state: any) => state.backet;

export const selectBacketProducts = createSelector(template, ({ products }) => products); 