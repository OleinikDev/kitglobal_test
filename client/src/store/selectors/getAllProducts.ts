import { createSelector } from 'reselect';

const template = (state: any) => state.getAllProducts;

export const selectAllProducts = createSelector(template, ({ products }) => products);

export const selectAllProductsStatus = createSelector(template, ({status}) => status);