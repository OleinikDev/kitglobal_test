import { ProductProps } from "@/pages/Shop/types"

export type BacketProps = {
  products: ProductProps[];
}