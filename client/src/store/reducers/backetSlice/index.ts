import { createSlice } from '@reduxjs/toolkit';
import { BacketProps } from './types';

const initialState: BacketProps = {
  products: []
};

export const backetSlice= createSlice({
  name: 'backet',
  initialState,
  reducers: {
    addProductToBacket: (state, { payload }) => {
      const existsInArray = state.products.some(prod => prod._id === payload._id);
      if (existsInArray) {
        return state;
      }
      return {
        ...state,
        products: [payload, ...state.products]
      }
    },
    incAmount: (state, { payload }) => {
      const product = state.products.find(prod => prod._id === payload._id);
      if (product) {
        return {
          ...state,
          products: state.products.map(prod => prod._id === payload._id
            ? { ...prod, items: prod.items + 1 }
            : prod
          )
        }
      }
    },
    decAmount: (state, { payload }) => {
      const product = state.products.find(prod => prod._id === payload._id);
      if (product) {
        return {
          ...state,
          products: state.products.map(prod => prod._id === payload._id
            ? { ...prod, items: prod.items - 1 }
            : prod
          )
        }
      }
    },
    deleteProductFromBacket: (state, { payload }) => ({
      ...state,
      products: state.products.filter(prod => prod._id !== payload)
    })
  },
});

export const backetReducer = backetSlice.reducer;
export const backetActions = backetSlice.actions;