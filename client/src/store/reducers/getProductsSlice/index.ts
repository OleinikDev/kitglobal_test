import { createSlice } from '@reduxjs/toolkit';
import { getProducts } from '../../actions/getProducts';
import { ProductsSliceProps } from './types';

const initialState: ProductsSliceProps = {
  products: [],
  status: false
};

export const getProductsSlice = createSlice({
  name: 'getAllProducts',
  initialState,
  reducers: {
    
  },
  extraReducers: {
    [getProducts.pending.toString()]: state => {
      state.status = false;
    },
    [getProducts.fulfilled.toString()]: (state, action) => {
      state.status = true;
      state.products = action.payload;
    },
    [getProducts.rejected.toString()]: (state) => {
      state.status = false;
    }
  }
});

export const getAllProductsReducer = getProductsSlice.reducer;
export const getAllProductsActions = getProductsSlice.actions;