import { createSlice } from '@reduxjs/toolkit';
import { ModalActionsProps } from './types';

const initialState: ModalActionsProps = {
  open: false
};

export const modalActions = createSlice({
  name: 'modal',
  initialState,
  reducers: {
    setModalOpen: (state) => void (state.open = true),
    setModalClose: (state) => void (state.open = false)
  },
});

export const modalStatusReducer = modalActions.reducer;
export const modalStatusActions = modalActions.actions;