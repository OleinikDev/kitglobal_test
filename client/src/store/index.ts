import { loadFromLocalStorage, saveToLocalStorage } from "@/utils/localStorage";
import { configureStore } from "@reduxjs/toolkit";
import { backetReducer, backetSlice } from "./reducers/backetSlice";
import { getAllProductsReducer, getProductsSlice } from "./reducers/getProductsSlice";
import { modalActions, modalStatusReducer } from "./reducers/modalActions";

const reducer = {
  [getProductsSlice.name]: getAllProductsReducer,
  [modalActions.name]: modalStatusReducer,
  [backetSlice.name]: backetReducer
}

const store = configureStore({
  reducer,
  devTools: true,
  preloadedState: loadFromLocalStorage()
})

store.subscribe(() => saveToLocalStorage(store.getState()));

export type AppDispatch = typeof store.dispatch

export default store;