import { API } from "@/api/api";
import { createAsyncThunk } from "@reduxjs/toolkit";

export const deleteProduct = createAsyncThunk('products/deleteOne', async (id, thunkApi) => {
  try {
    const data = await API.deleteProduct(id);
    return data;
  } catch (err) {
    return thunkApi.rejectWithValue(err);
  }
})