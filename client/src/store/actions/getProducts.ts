import { API } from "@/api/api";
import { createAsyncThunk } from "@reduxjs/toolkit";

export const getProducts = createAsyncThunk('products/getAll', async (_, thunkApi) => {
  try {
    const data = await API.getProducts();
    return data;
  } catch (err) {
    return thunkApi.rejectWithValue(err);
  }
})