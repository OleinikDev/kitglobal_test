import { API } from "@/api/api";
import { createAsyncThunk } from "@reduxjs/toolkit";

export const addProducts = createAsyncThunk('products/addOne', async (values, thunkApi) => {
  try {
    const data = await API.addProduct(values);
    return data;
  } catch (err) {
    return thunkApi.rejectWithValue(err);
  }
})