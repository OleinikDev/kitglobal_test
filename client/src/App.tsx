import AppWrapper from './components/AppWrapper'
import { RouterElement } from './utils/RouterElement'

function App() {

  return (
    <AppWrapper>
      <RouterElement />
    </AppWrapper>
  )
}

export default App
