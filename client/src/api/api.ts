import { FormValues } from "@/components/AddProductModal/types";
import { ProductProps } from "@/pages/Shop/types";
import axios, { AxiosResponse } from "axios";
const URl = import.meta.env.VITE_API_URL;

export class API {
  static async getProducts(): Promise<ProductProps[]> {
    const { data } = await axios.get(`${URl}api/products`);
    
    return data;
  }

  static async addProduct(params: FormValues): Promise<ProductProps> {
    const { data } = await axios.post(`${URl}api/products/add`, params);
    
    return data;
  }

  static async deleteProduct(id: string): Promise<AxiosResponse<any>> {
    const { data } = await axios.delete(`${URl}api/products/${id}`);
    
    return data;
  }
}