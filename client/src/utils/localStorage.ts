export const saveToLocalStorage = (state) => {
  try {
    const serialState = JSON.stringify(state)
    localStorage.setItem("reduxStore",serialState)
  } catch(e) {
    console.warn(e);
  }
}

export const loadFromLocalStorage = () => {
  try {
    const serialisedState = localStorage.getItem("reduxStore");
    if(serialisedState === null) return undefined;
    return JSON.parse(serialisedState);
  } catch(e) {
    console.warn(e);
    return undefined;
  }
}