import Loader from '@/components/Loader';
import { lazy, Suspense } from 'react'
import { useRoutes } from 'react-router-dom'

const Shop = lazy(() => import('../pages/Shop'));
const Backet = lazy(() => import('../pages/Backet'));

export function RouterElement() {
  const routes = [
    {
      path: '/',
      name: 'Shop',
      element: <Suspense fallback={<Loader />} ><Shop /></Suspense>
    },
    {
      path: '/backet',
      name: 'Backet',
      element: <Suspense fallback={<Loader />} ><Backet /></Suspense>
    },
  ]
  return useRoutes(routes)
}
